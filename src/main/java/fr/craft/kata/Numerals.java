package fr.craft.kata;

import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Stream;

public class Numerals {

  public static String toRoman(int arabic) {
    return Stream.iterate(new Numeral("", arabic), Numeral::decrease)
        .filter(Numeral::isResult)
        .findFirst()
        .get()
        .roman();
  }

  private record Numeral(String roman, int remaining) {
    private static final NavigableMap<Integer, String> CONVERSIONS = buildConversions();

    private static NavigableMap<Integer, String> buildConversions() {
      TreeMap<Integer, String> conversions = new TreeMap<>();

      conversions.put(1, "I");
      conversions.put(4, "IV");
      conversions.put(5, "V");

      return conversions;
    }

    boolean isResult() {
      return remaining == 0;
    }

    Numeral decrease() {
      Entry<Integer, String> highestKnownConversion = CONVERSIONS.floorEntry(remaining);

      return new Numeral(roman() + highestKnownConversion.getValue(), remaining - highestKnownConversion.getKey());
    }
  }
}
