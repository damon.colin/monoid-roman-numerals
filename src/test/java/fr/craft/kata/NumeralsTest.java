package fr.craft.kata;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class NumeralsTest {

  @Test
  void shouldConvertZeroToEmptyString() {
    assertThat(Numerals.toRoman(0)).isEmpty();
  }

  @Test
  void shouldConvertOneToI() {
    assertThat(Numerals.toRoman(1)).isEqualTo("I");
  }

  @Test
  void shouldConvertTwoToII() {
    assertThat(Numerals.toRoman(2)).isEqualTo("II");
  }

  @Test
  void shouldConvertFourToIV() {
    assertThat(Numerals.toRoman(4)).isEqualTo("IV");
  }

  @Test
  void shouldConvertFiveToV() {
    assertThat(Numerals.toRoman(5)).isEqualTo("V");
  }

  @Test
  void shouldConvertSixToVI() {
    assertThat(Numerals.toRoman(6)).isEqualTo("VI");
  }

  @Test
  void shouldConvertSevenToVII() {
    assertThat(Numerals.toRoman(7)).isEqualTo("VII");
  }

}
